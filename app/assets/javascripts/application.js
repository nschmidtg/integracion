// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require turbolinks
//= require s3_direct_upload
//= require jquery-ui
//= require wice_grid
//= require jquery-ui/datepicker
//= require_tree .


//= require_tree .
var docSubido = 0;
var vidSubido = 0;

var do_on_load = function() { 
  $('#s3_uploader1').S3Uploader(
    { 
      path: 'postulaciones/reportes/',
      remove_completed_progress_bar: false,
      progress_bar_target: $('#uploads_container1'),
      allow_multiple_files: false
    }
  );
  $('#s3_uploader1').bind('s3_upload_failed', function(e, content) {
    return alert(content.filename + ' failed to upload due to a timeout.');
  });

 $('#s3_uploader1').bind('s3_upload_complete', function(e, content) {
  $('#project_report_direct_upload_url').val(content.url);
  $('#project_report_file_name').val(content.filename);
  $('#project_report_file_path').val(content.filepath);
  $('#project_report_file_size').val(content.filesize);
  $('#project_report_content_type').val(content.filetype);
  docSubido = 1;
 if(vidSubido==1)
     $(this).prop("disabled",false);
 else
     $(this).prop("disabled",true);
});

};

var do_on_load2 = function() { 
  $('#s3_uploader2').S3Uploader(
    { 
      path: 'postulaciones/videos/',
      remove_completed_progress_bar: false,
      progress_bar_target: $('#uploads_container2'),
      allow_multiple_files: false
    }
  );
  $('#s3_uploader2').bind('s3_upload_failed', function(e, content) {
    return alert(content.filename + ' failed to upload due to a timeout.');
  });

 $('#s3_uploader2').bind('s3_upload_complete', function(e, content) {
    //insert any post progress bar complete stuff here.
  $('#project_video_direct_upload_url').val(content.url);
  $('#project_video_file_name').val(content.filename);
  $('#project_video_file_path').val(content.filepath);
  $('#project_video_file_size').val(content.filesize);
  $('#project_video_content_type').val(content.filetype);
     vidSubido = 1;
     if(docSubido==1)
         $('.rbutton').prop("disabled",false);
     else
         $('.rbutton').prop("disabled",true);
 });
};
$(document).ready(do_on_load);
$(document).ready(do_on_load2);
$(window).bind('page:change', do_on_load);
$(window).bind('page:change', do_on_load2);