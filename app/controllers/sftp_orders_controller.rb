class SftpOrdersController < ApplicationController
  
  require 'net/sftp'
  before_action :set_sftp_order, only: [:show, :edit, :update, :destroy]
  

  def self.connect
    @sftpConnect = Net::SFTP.start('moyas.ing.puc.cl', 'integra4', :password => 'M7yK.3?Rc')
    #json_pedidos_nuevos = []
    
    @sftpConnect.dir.foreach("/Pedidos") do |entry|
      
      #Obtiene todos los pedidos que no existan en nuestra base de datos local
	   	if(!SftpOrder.exists?(:name => entry.name) && entry.name.length > 3)
	 
	   		  address = "/Pedidos/" << entry.name
	   		  data = @sftpConnect.download!(address)
	   			data_json = Hash.from_xml(data).to_json
	   			hash = JSON.parse(data_json)

          SftpOrder.create(:name => entry.name)
		   		
		   		pedido = hash["xml"]["Pedido"]
		   		bo = BuyOrder.new()
          bo.order_id= pedido["oc"]
          bo.canal='ftp'
          bo.estado= 'creada'
          bo.cliente= pedido["cliente"]
          bo.proveedor= pedido["proveedor"]
          bo.sku= pedido["sku"]
          bo.fecha_entrega= pedido["fechaEntrega"]
          bo.cantidad= pedido["cantidad"]
          bo.precio_unitario= pedido["precioUnitario"]
          bo.save()
          puts pedido

          l=Log.new()
                  l.message="Orden de Compra creada vía FTP."
                  OdooController.cuentas(bo.precio_unitario.to_i*bo.cantidad.to_i,"Ventas","Cuentas por cobrar")
                  l.buy_order=bo
                  l.save()
           

          
                  #Se verifica si se aceptará o no la orden.
                  seAceptaOC = LogicController.recepcionarOrden(bo.order_id)
               
                  #Si es aceptada.
                  if seAceptaOC == true
                 
                     
                          destino = "0"
                    
                     
                      #Se procede a preparar el pedido. (Internamente, emitirá la factura también.)
                      LogicController.prepararPedido((bo.cantidad).to_s, (bo.sku).to_s, "ftp", (bo.order_id).to_s, destino , (bo.precio_unitario).to_s)
                 
                  #Sino.
                  else
                 
                      #El pedido es rechazado y "LogicController.recepcionarOrden" se encargó de avisar al cliente.
                      #Fin del proceso.
                
                  end 
		   		
		  end
		
	  end
	  
	   		
  end


  # GET /sftp_orders
  # GET /sftp_orders.json
  def index
    @sftp_orders = SftpOrder.all
  end

  # GET /sftp_orders/1
  # GET /sftp_orders/1.json
  def show
  end

  # GET /sftp_orders/new
  def new
    @sftp_order = SftpOrder.new
  end

  # GET /sftp_orders/1/edit
  def edit
  end

  # POST /sftp_orders
  # POST /sftp_orders.json
  def create
    @sftp_order = SftpOrder.new(sftp_order_params)

    respond_to do |format|
      if @sftp_order.save
        format.html { redirect_to @sftp_order, notice: 'Sftp order was successfully created.' }
        format.json { render :show, status: :created, location: @sftp_order }
      else
        format.html { render :new }
        format.json { render json: @sftp_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sftp_orders/1
  # PATCH/PUT /sftp_orders/1.json
  def update
    respond_to do |format|
      if @sftp_order.update(sftp_order_params)
        format.html { redirect_to @sftp_order, notice: 'Sftp order was successfully updated.' }
        format.json { render :show, status: :ok, location: @sftp_order }
      else
        format.html { render :edit }
        format.json { render json: @sftp_order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sftp_orders/1
  # DELETE /sftp_orders/1.json
  def destroy
    @sftp_order.destroy
    respond_to do |format|
      format.html { redirect_to sftp_orders_url, notice: 'Sftp order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sftp_order
      @sftp_order = SftpOrder.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sftp_order_params
      params.require(:sftp_order).permit(:name)
    end
    
end
