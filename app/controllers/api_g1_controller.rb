require 'httparty'
class ApiG1Controller < ApplicationController

#--------------PROVEEDOR--------------

    require 'uri'
    include HTTParty
    default_timeout 5

    #---Funciona---
    #Crear usuario grupo4 en la API del G1
    def self.new_user
        host = "http://integra1.ing.puc.cl"
        uri = URI(host+"/api/new_user")
        query = {"user" => "grupo4" , "pass" => "1234"}

        data = HTTParty.post(uri, :query => query)
        #hash = JSON.parse(data.body)
        
        #token = hash["token"]
        
        
		return data
    end
    
    #---Funciona---
    #Obetener el token para el usuario grupo4. Token = Z3J1cG80MTIzNA==
    def self.get_token
        host = "http://integra1.ing.puc.cl"
        uri = URI(host+"/api/get_token")
        query = {"user" => "grupo4" , "pass" => "1234"}
        data = HTTParty.get(uri, :query => query)
        hash = JSON.parse(data.body)
        
        token = hash["token"]
        
        return token
    end
    
    
    #---Conecta---
    #success: false
    #Notificar que "creamos" una orden para el G1
    def self.new_order(order_id)
        host = "http://integra1.ing.puc.cl"
        uri = URI(host+"/api/new_order")
        token = get_token
        query = {"order_id" => order_id , "token" => token}
        data = HTTParty.get(uri, :query => query)
        #hash = JSON.parse(data.body)
        
        return data
    end
    
    #---Conecta---
    #success: false
    #No pide token
    #Notificar que "aceptamos" una orden realizada por el G1
    def self.order_accepted(order_id)
        host = "http://integra1.ing.puc.cl"
        uri = URI(host+"/api/order_accepted")
        #token = get_token
        query = {"order_id" => order_id}
        data = HTTParty.put(uri, :query => query)
        #hash = JSON.parse(data.body)
        
        return data
    end
    
    #---Conecta---
    #success: false
    #Notificar que "cancelamos" una orden realizada por nosotros al G1
    def self.order_canceled(order_id)
        host = "http://integra1.ing.puc.cl"
        uri = URI(host+"/api/order_canceled")
        token = get_token
        query = {"order_id" => order_id, "token" => token}
        default_timeout 5
        data = HTTParty.delete(uri, :query => query)
        #hash = JSON.parse(data.body)
        
        return data
    end
    
    #---Conecta---
    #success: false
    #No pide token
    #Notificar que "rechazamos" una orden realizada por el G1
    def self.order_rejected(order_id)
        host = "http://integra1.ing.puc.cl"
        uri = URI(host+"/api/order_rejected")
        #token = get_token
        default_timeout 5
        query = {"order_id" => order_id}
        ####data = HTTParty.delete(uri, :query => query)
       
        ####return data
    end
    
    #---Conecta---
    #success: false
    #No utiliza token
    #Notificar que "creamos" una factura para el G1
    def self.new_invoice(invoice_id)
        host = "http://integra1.ing.puc.cl"
        uri = URI(host+"/api/new_invoice")
        #token = get_token
        query = {"invoice_id" => invoice_id}
        data = HTTParty.post(uri, :query => query)
        #hash = JSON.parse(data.body)
        
        return data
    end
    
    #---Conecta---
    #success: false
    #No utiliza token
    #Notificar que "rechazamos" una factura de el G1
    def self.invoice_rejected(invoice_id)
        host = "http://integra1.ing.puc.cl"
        uri = URI(host+"/api/invoice_rejected")
        #token = get_token
        query = {"invoice_id" => invoice_id}
        data = HTTParty.delete(uri, :query => query)
        
        return data
    end
    
    #---Error---
    #404
    #Notificar que "pagamos" una factura para el G1
    def self.invoice_paid(invoice_id)
        host = "http://integra1.ing.puc.cl"
        uri = URI(host+"/api/invoice_paid")
        token = get_token
        query = {"invoice_id" => invoice_id, "token" => token}
        data = HTTParty.post(uri, :query => query)
        #hash = JSON.parse(data.body)
        
        return data
    end
    


end
