class ApiG8Controller < ApplicationController

#--------------CLIENTE--------------

    #---Funciona---
    #Crear usuario grupo4 en la API del G8
    def self.new_user
        host = "http://integra8.ing.puc.cl"
        uri = URI(host+"/b2b/new_user")
        #headers = {"Content-Type" => "application/json", "Accept" => "application/json"}
        body = "{\"username\": \"grupo4\" , \"password\": \"1234\"}"
        puts body
        data = HTTParty.put(uri, :body => body)
        #hash = JSON.parse(data.body)
        
        #token = hash["token"]
        
		return data
    end
    
    #---Funciona---
    #Obetener el token para el usuario grupo4. Token = cambia en cada consulta: 9d3ff61073b9435c3324c9d41a31140f
    def self.get_token
        host = "http://integra8.ing.puc.cl"
        uri = URI(host+"/b2b/get_token")
        #headers = {"Content-Type" => "application/json", "Accept" => "application/json"}
        body = "{\"username\": \"grupo4\" , \"password\": \"1234\"}"
        data = HTTParty.post(uri, :body => body)
        hash = JSON.parse(data.body)
        
        token = hash["token"]
        
		return token
    end
    
    #---Error---
    #No sé cómo colocar el token
    #Notificar que "aceptamos" una orden realizada por el G5
    def self.order_accepted(order_id)
        host = "http://integra8.ing.puc.cl"
        uri = URI(host+"/b2b/order_accepted")
        #token = get_token
        #headers = {"Content-Type" => "application/json", "Accept" => "application/json"}
        body = "{\"order_id\": \""+order_id+"}"
        data = HTTParty.post(uri, :body => body)
        #hash = JSON.parse(data.body)
        
        #token = hash["token"]
        
		return data
    end
    
    #---Error---
    #No sé cómo colocar el token
    #Notificar que "rechazamos" una orden realizada por el G5
    def self.order_rejected(order_id)
        host = "http://integra8.ing.puc.cl"
        uri = URI(host+"/b2b/order_rejected")
        #token = get_token
        #headers = {"Content-Type" => "application/json", "Accept" => "application/json"}
        body = "{\"order_id\": \""+order_id+"}"
        data = HTTParty.post(uri, :body => body)
        #hash = JSON.parse(data.body)
        
        #token = hash["token"]
        
		return data
    end
    
    #---Error---
    #No sé cómo colocar el token
    #Notificar que "creamos" una factura para el G8
    def self.invoice_created(invoice_id)
        host = "http://integra8.ing.puc.cl"
        uri = URI(host+"/b2b/invoice_created")
        #token = get_token
        #headers = {"Content-Type" => "application/json", "Accept" => "application/json"}
        body = "{\"invoice_id\": \""+invoice_id+"}"
        data = HTTParty.post(uri, :body => body)
        #hash = JSON.parse(data.body)
        
        #token = hash["token"]
        
		return data
    end
    
    #---Error---
    #No sé cómo colocar el token
    #Notificar que "pagamos" una factura para el G8
    def self.invoice_paid(invoice_id)
        host = "http://integra8.ing.puc.cl"
        uri = URI(host+"/b2b/invoice_paid")
        #token = get_token
        #headers = {"Content-Type" => "application/json", "Accept" => "application/json"}
        body = "{\"invoice_id\": \""+invoice_id+"}"
        data = HTTParty.post(uri, :body => body)
        #hash = JSON.parse(data.body)
        
        #token = hash["token"]
        
		return data
    end
    
    #---Error---
    #No sé cómo colocar el token
    #Notificar que "rechazamos" una factura para el G8
    def self.invoice_rejected(invoice_id)
        host = "http://integra8.ing.puc.cl"
        uri = URI(host+"/b2b/invoice_rejected")
        #token = get_token
        #headers = {"Content-Type" => "application/json", "Accept" => "application/json"}
        body = "{\"invoice_id\": \""+invoice_id+"}"
        data = HTTParty.post(uri, :body => body)
        #hash = JSON.parse(data.body)
        
        #token = hash["token"]
        
		return data
    end
    

end
