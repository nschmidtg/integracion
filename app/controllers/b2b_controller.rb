class B2bController < ApplicationController

	def new_user
		@user = params[:username].to_s
		@password = params[:password].to_s


		@a = ApiKey.new()
		@a.username = @user
		@a.password = @password

		@a.save()
		
		respond_to do |format|
      if @a.save
        #format.html { redirect_to @a, notice: 'User was successfully created.' }
        format.json { 
                   render :json =>{
                   		:success => :true,
                   		:message => "Usuario creado exitosamente.",
                   		:token => @a.access_token
                   }
                  }
                  format.html { render :new }
      else
        #format.html { render :new }
        format.json { render json: @a.errors, status: :unprocessable_entity }
      end
    end
	end

	def get_token
		@user = params[:username]
		@password = params[:password]

		@api_key = ApiKey.find_by_username(params[:username])

		respond_to do |format|
			if @user != nil && @password != nil 
				if @api_key != nil
					if @password == @api_key.password
						 format.json { 
			                   render :json =>{
			                   		:success => :true,
			                   		:token => @api_key.access_token
			                   }
			                  }
			      else
			        #format.html { render :new }
			        format.json { 
			                   render :json =>{
			                   		:success => :false,
			                   		:message => "Contraseña inválida."
			                   }
			                  }
			      end
			  else
			  	 format.json { 
			                   render :json =>{
			                   		:success => :false,
			                   		:message => "Nombre de usuario inválido."
			                   }
			                  }
			  end
			else
				if @user == nil
					format.json { 
			                   render :json =>{
			                   		:success => :false,
			                   		:message => "username no ingresado."
			                   }
			                  }
			    else
			    	format.json { 
			                   render :json =>{
			                   		:success => :false,
			                   		:message => "password no ingresada."
			                   }
			                  }
			    end
			end
    end
	end
end
