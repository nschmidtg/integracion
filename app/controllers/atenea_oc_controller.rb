class AteneaOcController < ApplicationController
    
    #####################################
    ## TODOS LOS PARAMETROS SON STRING ##
    #####################################
    #####################################
    ##       DEVUELVE 202 o 400        ## 
    #####################################
    def self.getUrl()
       return 'http://chiri.ing.puc.cl:8080/Grupo4/integra4/orden'
       #return 'http://chiri.ing.puc.cl:8080/Grupo4/integra4/orden'
    end
    
    #Funcionando.
    def self.crearOC(canal, cantidad, sku, proveedor , cliente, fechaEntrega, precioUnitario)
        
        url = getUrl() + '/crear'
        
        #Se realiza la consulta.
        response = HTTParty.put(url,
        :body => { :canal => canal, :cantidad => cantidad, :sku => sku,
        :proveedor => proveedor, :precioUnitario => precioUnitario, 
        :cliente => cliente, :fechaEntrega => fechaEntrega}.to_json,
        :headers => { "Content-Type" => "application/json"})
        
        puts "----------------------Parametros------------------------"
        puts canal
        puts cantidad
        puts sku
        puts proveedor
        puts cliente
        puts fechaEntrega
        puts precioUnitario
        puts "----------------------END------------------------"
        puts response.body
        puts "----***----"
        body = JSON.parse(response.body)
        
        
        return [response.code, body]
        
    end 
    
    #Funcionando.
    #id 557b29b916e7650300ede489 <- id recepcionada.
    def self.recepcionarOC(idOC)
        
        url = getUrl() + '/recepcionar/' + idOC
        
        #Se realiza la consulta.
        response = HTTParty.post(url,
        :body => { }.to_json,
        :headers => { "Content-Type" => "application/json"})
        
        body = JSON.parse(response.body)
        
        return [response.code, body]
        
    end 
    
    #Funcionando.
    #id 557b261416e7650300ede488 <- id rechazada.
    def self.rechazarOC(idOC, motivoRechazo) 
        
        url = getUrl() + '/rechazar/' + idOC
        
        #Se realiza la consulta.
        response = HTTParty.post(url,
        :body => { :rechazo => motivoRechazo }.to_json,
        :headers => { "Content-Type" => "application/json"})
        
        body = JSON.parse(response.body)
        
        return [response.code, body]
        
    end 
    
    #Funcionando.
    # id 557b2aae16e7650300ede48a <- id anulada.
    def self.anularOC(idOC, motivoAnulacion)
        
        url = getUrl() + '/anular/' + idOC
        
        #Se realiza la consulta.
        response = HTTParty.delete(url,
        :body => { :anulacion => motivoAnulacion }.to_json,
        :headers => { "Content-Type" => "application/json"})
        
        body = JSON.parse(response.body)
        
        return [response.code, body]

    end 
    
    #Funcionando.
    def self.obtenerOC(idOC)
        
        url = getUrl() + '/obtener/' + idOC
        
        #Se realiza la consulta.
        response = HTTParty.get(url,
        :body => { }.to_json,
        :headers => { "Content-Type" => "application/json"})
        
        body = JSON.parse(response.body)
        
        return [response.code, body]
        
    end 
    
end
