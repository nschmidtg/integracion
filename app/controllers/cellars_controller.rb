class CellarsController < ApplicationController
    
    def self.link()
        
        #return 'http://integracion-2015-dev.herokuapp.com/bodega'
        return 'http://integracion-2015-prod.herokuapp.com/bodega'
    end
    
    
    def self.hmacShai1(s) #s debe ser un string ""
    require 'openssl'
    require "base64"
    #clave = "8Ej3xJCdMrDQe2D" #Development
    clave = "KqcyeWMPlClWBl" #Produccion
    hmac  = OpenSSL::HMAC.digest('sha1', clave, s)

    return Base64.encode64(hmac).split("\n")[0]


    end
    
    def self.get_almacenes_id(bodega) #Se debe poner el nombre de la bodega ("despacho","recepcion","pulmon","bodega1","bodega2")
    	bod2 = false
    	@respuesta = HTTParty.get(link()+'/almacenes/', headers: {"Authorization" => "INTEGRACION grupo4:"+hmacShai1("GET")})
        if bodega == "despacho" || bodega == "recepcion" || bodega == "pulmon"
            @respuesta.each do |almacen|
    	        if almacen[bodega]
    	            return almacen["_id"]
    	        end
    	    end
        end
        
        if bodega == "bodega1"
             @respuesta.each do |almacen|
    	        if !almacen["pulmon"] && !almacen["recepcion"] && !almacen["despacho"]
    	            return almacen["_id"]
    	        end
    	    end
        end
        if bodega == "bodega2"
             @respuesta.each do |almacen|
                if !almacen["pulmon"] && !almacen["recepcion"] && !almacen["despacho"] && bod2
    	            return almacen["_id"]
    	        end
    	        if !almacen["pulmon"] && !almacen["recepcion"] && !almacen["despacho"]
    	            bod2 = true
    	        end
    	        
    	    end
        end
    end
    
    
    def self.get_capacidad(bodega) #Poner el nombre de la bodega ("pulmon","despacho","recepcion","bodega1","bodega2")
    	@respuesta = HTTParty.get(link()+'/almacenes/', headers: {"Authorization" => "INTEGRACION grupo4:"+hmacShai1("GET")})
        
    	@respuesta.each do |almacen|
    	    if almacen["_id"] == get_almacenes_id(bodega)
    	        return (almacen["totalSpace"]-almacen["usedSpace"])
    	    end
    	end
    end
   

    def self.get_capacidad_usada(bodega) #Poner el nombre de la bodega ("pulmon","despacho","recepcion","bodega1","bodega2")
    	@respuesta = HTTParty.get(link()+'/almacenes/', headers: {"Authorization" => "INTEGRACION grupo4:"+hmacShai1("GET")})
        
    	@respuesta.each do |almacen|
    	    if almacen["_id"] == get_almacenes_id(bodega)
    	        return almacen["usedSpace"]
    	    end
    	end
    end
    
    
    def self.getCantidadSKU(idSKU,cellar) #Devuelve cantidad de sku de cierta bodega El idSKU debe ser un string!
        
        total = 0
        idBodega = get_almacenes_id(cellar).to_s
        
        skus = HTTParty.get(link() + '/skusWithStock?almacenId='+idBodega, headers: {"Authorization" => "INTEGRACION grupo4:"+hmacShai1("GET"+idBodega)}) 
            skus.each do |sku|
                if sku["_id"] == idSKU
                 total+=sku["total"]
                end
            end
        
        return total
    end
    

    def self.reordenar() #reordena los almacenes de despacho, recepción y pulmon
        origenes = ["recepcion","pulmon"]
         
        origenes.each do |origen|
            puts "Vaciamos la bodega: " + origen
            idOrigen = get_almacenes_id(origen).to_s
            puts idOrigen
            numeroProductosOrigen = get_capacidad_usada(origen)
            if origen == "pulmon"
                bodegas = ["bodega1","bodega2"]
            else
                bodegas =["bodega1","bodega2","pulmon"]
            end
            
            bodegas.each do |bodega| #para cada una de las bodegas
                puts "Tratamos de dejarlo en: " + bodega.to_s
                capacidadBodega = get_capacidad(bodega)
                
                puts "entramos al while"
                idBodega = get_almacenes_id(bodega).to_s
                skus = HTTParty.get(link() + '/skusWithStock?almacenId='+idOrigen, headers: {"Authorization" => "INTEGRACION grupo4:"+hmacShai1("GET"+idOrigen)}) 
                    puts skus
                    skus.each do |sku| #Para cada sku
                    puts "encontramos un sku"
                        @detalle = HTTParty.get(link() + '/stock?almacenId='+idOrigen+'&sku='+sku["_id"], headers: {"Authorization" => "INTEGRACION grupo4:"+hmacShai1("GET"+idOrigen+sku["_id"])}) 
                        @detalle.each do |product| #para cada producto de cierto sku de bodega origen
                                puts "vamos a moveeer"
                                resp = HTTParty.post(link() + "/moveStock",
                                :body => {:productoId => product["_id"],:almacenId => idBodega},
                                :headers => { "Authorization" => "INTEGRACION grupo4:"+hmacShai1("POST"+product["_id"]+idBodega)})
                                puts resp
                        end
                    numeroProductosOrigen = get_capacidad_usada(origen)
                    capacidadBodega = get_capacidad(bodega)
                    if numeroProductosOrigen == 0 || capacidadBodega ==0
                       break 
                    end    
            
                    
                    
                end    
            end
        end
    end
    
    
    def self.send_products_b2b(productId, idBodegaDestino, cantidad) #Todo en string!, retorna false si no se puede
            contador = 0
            idDespacho = get_almacenes_id("despacho")
            if (getCantidadSKU(productId,"pulmon").to_i + getCantidadSKU(productId,"bodega1").to_i + getCantidadSKU(productId,"bodega2").to_i)>cantidad.to_i && get_capacidad("despacho")>cantidad.to_i
                
                bodegas = ["pulmon","bodega1","bodega2"]
                bodegas.each do |bodega| #para cada bodega
                   
                    idBodega = get_almacenes_id(bodega)
                    cantidadSKU = getCantidadSKU(productId,bodega)
                    
                    while cantidadSKU >0 && contador < cantidad.to_i
                        detalle = HTTParty.get(link() + '/stock?almacenId=' + idBodega.to_s + '&sku=' + productId.to_s, headers: {"Authorization" => "INTEGRACION grupo4:"+hmacShai1("GET"+idBodega.to_s+productId.to_s)}) 
                        #Acá se mueve todo a la bodega de despacho
                        detalle.each do |producto|
                            respuesta = HTTParty.post(link() + "/moveStock",
                            :body => {:productoId => producto["_id"],:almacenId => idDespacho},
                            :headers => { "Authorization" => "INTEGRACION grupo4:" + hmacShai1("POST" + producto["_id"].to_s + idDespacho.to_s)})
                            puts respuesta
                            if respuesta["error"].to_s != ""
                                puts respuesta
                                return [false,contador]
                            end
                            ###########ESTA PARTE HAY QUE PROBARLA CON OTRO GRUPO
                            respuesta2 = HTTParty.post(link() + "/moveStockBodega",
                            :body => {:productoId => producto["_id"],:almacenId => idBodegaDestino},
                            :headers => { "Authorization" => "INTEGRACION grupo4:" + hmacShai1("POST" + producto["_id"].to_s + idBodegaDestino.to_s)})
                            puts "aksdkljasd"
                            puts respuesta2
                            if respuesta2["error"].to_s != ""
                                return [false,contador]
                            end
                            contador +=1
                            cantidadSKU = getCantidadSKU(productId,bodega)
                        end
                    end
                end
            else
                return [false,contador]
            end
            #Acá ya todos los productos están despachados
            return [true,contador]
    end
    

    def self.send_products_cliente(productId,direction, precio,ocId,cantidad) #Todo en string!, retorna false si no se puede
            contador = 0
            idDespacho = get_almacenes_id("despacho")
            if (getCantidadSKU(productId,"pulmon").to_i + getCantidadSKU(productId,"bodega1").to_i + getCantidadSKU(productId,"bodega2").to_i)>cantidad.to_i && get_capacidad("despacho")>cantidad.to_i
                
                bodegas = ["pulmon","bodega1","bodega2"]
                bodegas.each do |bodega| #para cada bodega
                   
                    idBodega = get_almacenes_id(bodega)
                    cantidadSKU = getCantidadSKU(productId,bodega)
                     
                    while cantidadSKU >0 && contador < cantidad.to_i
                        detalle = HTTParty.get(link() + '/stock?almacenId=' + idBodega.to_s + '&sku=' + productId.to_s, headers: {"Authorization" => "INTEGRACION grupo4:"+hmacShai1("GET"+idBodega.to_s+productId.to_s)}) 
                        #Acá se mueve todo a la bodega de despacho
                        detalle.each do |producto|
                            if bodega == "pulmon"
                                if get_capacidad("bodega1") > 0
                                    idBodegaAux = get_almacenes_id("bodega1")
                                elsif get_capacidad("bodega2") > 0
                                    idBodegaAux = get_almacenes_id("bodega2")
                                else
                                    return false, contador
                                end
                                respuesta = HTTParty.post(link() + "/moveStock",
                                :body => {:productoId => producto["_id"],:almacenId => idBodegaAux},
                                :headers => { "Authorization" => "INTEGRACION grupo4:" + hmacShai1("POST" + producto["_id"].to_s + idBodegaAux.to_s)})
                                puts respuesta
                            
                                if respuesta["error"].to_s != ""
                                    puts respuesta
                                    return [false,contador]
                                end
                            end
                            
                            
                            respuesta = HTTParty.post(link() + "/moveStock",
                            :body => {:productoId => producto["_id"],:almacenId => idDespacho},
                            :headers => { "Authorization" => "INTEGRACION grupo4:" + hmacShai1("POST" + producto["_id"].to_s + idDespacho.to_s)})
                            idProd = respuesta["_id"]
                            if respuesta["error"].to_s != ""
                                puts respuesta
                                return [false,contador]
                            end
                            ###########MANDANDOLA A EXTERNOS
                            puts "sadasldkhasudban dkasjhn"
                            puts direction
                            puts precio.to_i
                            puts ocId
    
                            respuesta2 = HTTParty.delete(link() + "/stock",
                            :body => {:productoId => idProd.to_s,:direccion => direction,:precio => precio.to_i,:ordenDeCompraId => ocId},
                            :headers => { "Authorization" => "INTEGRACION grupo4:" + hmacShai1("DELETE" + idProd.to_s + direction.to_s + precio.to_s + ocId)})
                            puts respuesta2
                            if respuesta2["error"].to_s != ""
                                return [false,contador]
                            end
                            contador +=1
                            cantidadSKU = getCantidadSKU(productId,bodega)
                        end
                    end
                
                end
            else
                return [false,contador]
            end
            #Acá ya todos los productos están despachados
            return [true,contador]
    end
    
    def self.mover_a_despacho(sku,cantidad)
            idDespacho = get_almacenes_id("despacho")
            if (getCantidadSKU(productId,"pulmon").to_i + getCantidadSKU(productId,"bodega1").to_i + getCantidadSKU(productId,"bodega2").to_i)>cantidad.to_i && get_capacidad("despacho")>cantidad.to_i
                
                bodegas = ["pulmon","bodega1","bodega2"]
                bodegas.each do |bodega| #para cada bodega
                   
                    idBodega = get_almacenes_id(bodega)
                    cantidadSKU = getCantidadSKU(productId,bodega)
                     
                    while cantidadSKU >0 && contador < cantidad.to_i
                        detalle = HTTParty.get(link() + '/stock?almacenId=' + idBodega.to_s + '&sku=' + productId.to_s, headers: {"Authorization" => "INTEGRACION grupo4:"+hmacShai1("GET"+idBodega.to_s+productId.to_s)}) 
                        #Acá se mueve todo a la bodega de despacho
                        detalle.each do |producto|
                            
                            if bodega == "pulmon"
                                if get_capacidad("bodega1") > 0
                                    idBodegaAux = get_almacenes_id("bodega1")
                                elsif get_capacidad("bodega2") > 0
                                    idBodegaAux = get_almacenes_id("bodega2")
                                else
                                    return false, contador
                                end
                                respuesta = HTTParty.post(link() + "/moveStock",
                                :body => {:productoId => producto["_id"],:almacenId => idBodegaAux},
                                :headers => { "Authorization" => "INTEGRACION grupo4:" + hmacShai1("POST" + producto["_id"].to_s + idBodegaAux.to_s)})
                                puts respuesta
                            
                                if respuesta["error"].to_s != ""
                                    puts respuesta
                                    return [false,contador]
                                end
                            end
                            respuesta = HTTParty.post(link() + "/moveStock",
                            :body => {:productoId => producto["_id"],:almacenId => idDespacho},
                            :headers => { "Authorization" => "INTEGRACION grupo4:" + hmacShai1("POST" + producto["_id"].to_s + idDespacho.to_s)})
                            puts respuesta
                            if respuesta["error"].to_s != ""
                                puts respuesta
                                return [false,contador]
                            end
                        end
                    end
                end
            else
                return [false,contador]
            end
            return [true,contador]
    end
    
    
    def self.producir(productId,cantidad) ##Product id es un string
        
        if productId != "13" && productId != "19" && productId != "21" && productId != "22" && productId != "23" && productId != "24"
            return false
        end
       
       #Verificamos que sean multiplos de los lotes y los movemos a despacho
       
        if productId == "22"
            if (cantidad.to_i % 380) == 0
                movido = moveer_a_despacho("6",((cantidad.to_i)*LogicController.getInfoSKU(productId)[4]).to_s)[0]
            else
                return false
            end
        elsif productId =="23"
            if (cantidad.to_i % 309) == 0
                movido = moveer_a_despacho("8",((cantidad.to_i)*LogicController.getInfoSKU(productId)[4]).to_s)[0]
                return false
            end
        elsif productId =="24"
            if (cantidad.to_i % 444) == 0
                movido = moveer_a_despacho("33",((cantidad.to_i)*LogicController.getInfoSKU(productId)[4]).to_s)[0]
            else
                return false
            end
        else
            movido = true
        end
        
        if !movido
            return false
        end
        
        respuesta = HTTParty.get(link() + '/fabrica/getCuenta', headers: {"Authorization" => "INTEGRACION grupo4:" + hmacShai1("GET").to_s}) 
        cuentaBodega = respuesta["cuentaId"]
        puts "## Cuenta bodega: " + cuentaBodega.to_s
        monto = LogicController.getInfoSKU(productId)[2] * cantidad
        puts "#########MONTO" + monto.to_s
        trx = BankController.transferir(monto,cuentaBodega) 
        
        
        
        puts "##########sku = " + productId.to_s + " trxId = " + trx[1]["id"].to_s + " cantidad = " + cantidad.to_s
        respuesta2 = HTTParty.put(link() + "/fabrica/fabricar",
                            :body => {:sku => productId,:trxId => trx[1]["id"],:cantidad => cantidad},
                            :headers => { "Authorization" => "INTEGRACION grupo4:"+hmacShai1("PUT"+productId.to_s+cantidad.to_s+trx[1]["id"].to_s)})
        puts "RESPONSEEEE: " + respuesta2.to_s                    
        
        if respuesta2["error"].to_s != ""
            return false
        end
        return true
    end
    
    
    #Este método hay que llamarlo cada 5 horas
    def self.abastecerse() #Vamos a tratar de tener 1500 de cada producto //Ver bien cuando lo llamams porque podría hacer muchos requests a otros grupos a fabrica
        productos = ["6","8","33","13","19","21","22","23","24"]
        productos.each do |sku|
            cantidadSKU = getCantidadSKU(sku,"bodega1")+getCantidadSKU(sku,"bodega2")+getCantidadSKU(sku,"pulmon")
            if(cantidadSKU < 1500)
                if sku == "13" || sku == "19" || sku == "21" || sku == "22" || sku == "23" || sku == "24"
                    cantidad = ((1500-cantidadSKU)/LogicController.getInfoSKU(sku)[1]).to_i * LogicController.getInfoSKU(sku)[1]
                    puts "cantidad: " + cantidad.to_s
                    puts "checkpoint 1.0 " + cantidad.to_s
                    producir(sku,cantidad)
                elsif sku == "6" || sku == "8" || sku == "33"
                    cantidad = 1500 - cantidadSKU
                    LogicController.comprarAgrupos(sku,cantidad)
                end
            end
        end
    end
    
    
end
