class FacturaController < ApplicationController

    def self.recibirOrden(idOC)
       #ESTE ES EL METODO QUE SE TIENE QUE 
        #LLAMAR CON LA ID RETORNADA POR atenea DE LA ORDEN DE COMPRA
        
        json = AteneaOcController.recepcionarOC(idOC)
        if json[0] == 200
            canal = json[1]["canal"]
        end
        
        if canal == "e-commerce" #ojo!!
           # if cupon
            #else
           # end
        end
        
        self.firmarEmitir(idOC)
    end


    def self.firmarEmitir(idOC)
        
        json = self.emitirFactura(idOC)

        if json[0] == 200
            cliente = json[1]["cliente"]
            proveedor = json[1]["proveedor"]
            bruto = json[1]["bruto"]
            iva = json[1]["iva"]
            total = json[1]["total"]
            oc = json[1]["oc"]
            _id = json[1]["_id"]
            estado = json[1]["estado"]

            @factura = Factura.new()
            @factura.id_fact = _id
            @factura.proveedor = proveedor
            @factura.valor_bruto = bruto
            @factura.iva =iva
            @factura.oc = oc
            @factura.cliente = cliente
            @factura.valor_total = total
            @factura.estado_pago = estado

            @factura.save()

            if cliente.include? "3"
                ApiG3Controller.new_invoice(_id)
            elsif cliente.include? "5"
                ApiG5Controller.new_invoice(_id)
            elsif cliente.include? "8"
                ApiG8Controller.new_invoice(_id)
            else
                #Si estamos acá hay algo mal...
            end
        end

        #acá se deberia hacer algo con la contabilidad, pero ya terminó en verdad..
        return json
    end

    
    #####################################
    ## TODOS LOS PARAMETROS SON STRING ##
    #####################################
    #####################################
    ##       DEVUELVE 202 o 400        ## 
    #####################################
    
    
    # Funcionando.
    def self.emitirFactura(idOC)
        
        url = 'http://moyas.ing.puc.cl:8080/Grupo4/integra4/factura/'
        
        response = HTTParty.put(url,
        :body => { :id => idOC}.to_json,
        :headers => { "Content-Type" => "application/json"})
        
        body = JSON.parse(response.body)
        
        return [response.code, body]
        
    end     
    
    #Funcionando.
    def self.obtenerFactura(idFactura)
        
        url = 'http://moyas.ing.puc.cl:8080/Grupo4/integra4/factura/'
        url = url + idFactura.to_s
        response = HTTParty.get(url)
        
        body = JSON.parse(response.body)
        
        return [response.code, body]
        
    end     
    
    #Funcionando.
    #id 557b2dc916e7650300ede48d <- id pagada.
    def self.pagarFactura(idFactura)
    
        url = 'http://moyas.ing.puc.cl:8080/Grupo4/integra4/factura/pay'

        response = HTTParty.post(url,
        :body => { :id => idFactura}.to_json,
        :headers => { "Content-Type" => "application/json"})
        
        body = JSON.parse(response.body)
        
        return [response.code, body]
    
    end     
    
    #Funcionando.
    #id 557b2f1f16e7650300ede48f <- id rechazada.
    def self.rechazarFactura(idFactura, motivo)
        
        #Se realiza la consulta.
        url = 'http://moyas.ing.puc.cl:8080/Grupo4/integra4/factura/reject'

        response = HTTParty.post(url,
        :body => { :id => idFactura, :motivo => motivo}.to_json,
        :headers => { "Content-Type" => "application/json"})
        
        body = JSON.parse(response.body)
        
        return [response.code, body]
        
    end     
    
    #Funcionando.
    #id 557b2fa716e7650300ede491 <- id anulada.
    def self.anularFactura(idFactura, motivo)
       
        #Se realiza la consulta.
        url = 'http://moyas.ing.puc.cl:8080/Grupo4/integra4/factura/cancel'

        response = HTTParty.post(url,
        :body => { :id => idFactura, :motivo => motivo}.to_json,
        :headers => { "Content-Type" => "application/json"})
        
        body = JSON.parse(response.body)
        
        return [response.code, body]
        
    end
    
    #Funcionando.
    #id 5592f23fea9b7c0300571220 
    def self.emitirBoleta(cliente, total)
        
        url = 'http://moyas.ing.puc.cl:8080/Grupo4/integra4/factura/boleta'
        
        response = HTTParty.put(url,
        :body => { :proveedor => '55648ad2f89fed0300524ff8', :cliente => cliente, :total => total}.to_json,
        :headers => { "Content-Type" => "application/json"})
        
        body = JSON.parse(response.body)
        
        return [response.code, body]
        
    end

end
