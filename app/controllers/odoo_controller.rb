class OdooController < ApplicationController
    
    require "xmlrpc/client"

 def self.cuentas(plata, mas, menos)
begin
url = "http://moyas.ing.puc.cl:8070"
db = "INTEGRA4"
username = "admin"
password = "M7yK.3?Rc"

common = XMLRPC::Client.new2("#{url}/xmlrpc/2/common")
uid = common.call('authenticate', db, username, password, {})

models = XMLRPC::Client.new2("#{url}/xmlrpc/2/object").proxy

###########################

monto = plata.to_i

###########################
#Creo la Journal Entry

##Cuenta a subir la plata

#cuenta_mas = 'Banco'
#cuenta_menos = 'Cuentas por cobrar'
## 'Banco'
## 'Cuentas por cobrar'
## 'Cuentas por pagar'
## 'Ventas'
## 'Compras'


cuenta_mas = mas
cuenta_menos = menos
#cuenta_mas = 'Cuentas por cobrar'
#cuenta_menos = 'Compras'

######################################################
## Obtiene el id del Journal por default

v_id_journal = models.execute_kw(db, uid, password,
    'account.journal', 'search',
    [[['name', '=', 'Stock Journal']]],
    {limit: 1})



######################################################
##Saco la ID del periodo
v_id_periodo = models.execute_kw(db, uid, password,
    'account.period', 'search',
    [[['date_start', '=', '2015-07-01'],['date_stop', '=', '2015-07-31']]])


######################################################
##Obtiene el id de las cuentas banco
v_id_cuenta1  = models.execute_kw(db, uid, password,
    'account.account', 'search',
    [[['name', '=', cuenta_mas]]])

v_id_cuenta2  = models.execute_kw(db, uid, password,
    'account.account', 'search',
    [[['name', '=', cuenta_menos]]])


######################################################
#Crea el Journal entry (transaccion)

v_id_journal_entry = models.execute_kw(db, uid, password,
	'account.move', 'create',[{ 
	journal_id: v_id_journal[0],
}])


######################################################
##Crea los Journal Item

##Aumenta una cuenta
v_id_linea1 = models.execute_kw(db, uid, password,
	'account.move.line', 'create',[{ 
	account_id: v_id_cuenta1[0],
	journal_id: v_id_journal[0],
	period_id: v_id_periodo[0],
	date: '2015-07-02',
	name: 'Transaccion credito',
	debit: monto,
	move_id: v_id_journal_entry,
}])

#Disminuye una cuenta
v_id_linea2 = models.execute_kw(db, uid, password,
	'account.move.line', 'create',[{ 
	account_id: v_id_cuenta2[0],
	journal_id: v_id_journal[0],
	period_id: v_id_periodo[0],
	date: '2015-07-02',
	name: 'Transaccion debito',
	credit: monto,
	move_id: v_id_journal_entry,
}])
rescue
end
    
    
end
    
end
