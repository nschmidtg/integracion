class ApiG3Controller < ApplicationController

#--------------CLIENTE--------------

require 'uri'

    #---Funciona---
    #No usan :headers
    #Crear usuario grupo4 en la API del G3
    def self.new_user
        host = "http://integra3.ing.puc.cl"
        uri = URI(host+"/b2b/new_user/")
        #headers = {"Content-Type" => "application/json", "Accept" => "application/json"}
        body = {"username" => "grupo4" , "password" => "1234"}
        data = HTTParty.put(uri, :body => body)
        #hash = JSON.parse(data.body)
        
        #token = hash["token"]
        
        
		return data
    end
    
    #---Funciona---
    #No usan :headers
    #Obetener el token para el usuario grupo4. Token = 73a0c31a9aa678aaf0817c44d4e48af914e8d275
    def self.get_token
        host = "http://integra3.ing.puc.cl"
        uri = URI(host+"/b2b/get_token/")
        #headers = {"Content-Type" => "application/json", "Accept" => "application/json"}
        body = {"username" => "grupo4" , "password" => "1234"}
        data = HTTParty.post(uri, :body => body)
        hash = JSON.parse(data.body)
        
        token = hash["token"]
        
        return token
    end
    
    #---Funciona---
    #message: Muchas gracias por avisar
    #Notificar que "aceptamos" una orden realizada por el G3
    def self.order_accepted(order_id)
        host = "http://integra3.ing.puc.cl"
        uri = URI(host+"/b2b/order_accepted/")
        token = get_token
        headers = {"Authorization" =>"Token "+ token}
        body = {"order_id" => order_id}
        data = HTTParty.post(uri, :headers => headers, :body => body)
        
        return data
    end
    
    #---Funciona---
    #message: Muchas gracias por avisar.
    #Notificar que "rechazamos" una orden realizada por el G3
    def self.order_rejected(order_id)
        host = "http://integra3.ing.puc.cl"
        uri = URI(host+"/b2b/order_rejected/")
        token = get_token
        headers = {"Authorization" =>"Token "+ token}
        body = {"order_id" => order_id}
        data = HTTParty.post(uri, :headers => headers, :body => body)
        
        return data
    end
    
    #---Funciona---
    #message: Muchas gracias por avisar.
    #Notificar que "creamos" una factura para el G3
    def self.invoice_created(invoice_id)
        host = "http://integra3.ing.puc.cl"
        uri = URI(host+"/b2b/invoice_created/")
        token = get_token
        headers = {"Authorization" =>"Token "+ token}
        body = {"invoice_id" => invoice_id}
        data = HTTParty.post(uri, :headers => headers, :body => body)
        
        return data
    end
    
    #---Funciona---
    #message: Muchas gracias por avisar.
    #Notificar que "pagamos" una factura para el G1
    def self.invoice_paid(invoice_id)
        host = "http://integra3.ing.puc.cl"
        uri = URI(host+"/b2b/invoice_paid/")
        token = get_token
        headers = {"Authorization" =>"Token "+ token}
        body = {"invoice_id" => invoice_id}
        data = HTTParty.post(uri, :headers => headers, :body => body)
        
        return data
    end
    
    #---Funciona---
    #message: Muchas gracias por avisar.
    #Notificar que "rechazamos" una factura de el G1
    def self.invoice_rejected(invoice_id)
        host = "http://integra3.ing.puc.cl"
        uri = URI(host+"/b2b/invoice_rejected/")
        token = get_token
        headers = {"Authorization" =>"Token "+ token}
        body = {"invoice_id" => invoice_id}
        data = HTTParty.post(uri, :headers => headers, :body => body)
        
        return data
    end
    
end
