class ApiG5Controller < ApplicationController

#--------------CLIENTE--------------

    #---Conecta---
    #"respuesta": "Bad request error, Id inválido"
    #Notificar que "aceptamos" una orden realizada por el G5
    def self.order_accepted(order_id)
        host = "http://integra5.ing.puc.cl"
        uri = URI(host+"/b2b/order_accepted/")
        query = {"order_id" => order_id}
        data = HTTParty.post(uri, :query => query)
        
        return data
    end
    
    #---Conecta---
    #"respuesta": "Bad request error, Id inválido"
    #Notificar que "aceptamos" una orden realizada por el G5
    def self.order_rejected(order_id)
        host = "http://integra5.ing.puc.cl"
        uri = URI(host+"/b2b/order_rejected/")
        query = {"order_id" => order_id}
        data = HTTParty.post(uri, :query => query)
        
        return data
    end


end
