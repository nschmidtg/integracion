class BankController < ApplicationController
    
    #####################################
    ## TODOS LOS PARAMETROS SON STRING ##
    #####################################
    #####################################
    ##       DEVUELVE 200 o 400        ## 
    #####################################
    
    
    #Funcionando.
    def self.transferir(monto, cuenta_destino) 
    
        url = 'http://moyas.ing.puc.cl:8080/Grupo4/integra4/banco/trx'
        
        #Se realiza la consulta.
        response = HTTParty.put(url,
        :body => { :monto => monto, :origen => "55648ad3f89fed0300525004", :destino => cuenta_destino }.to_json,
        :headers => { "Content-Type" => "application/json"})
        begin
        body = JSON.parse(response.body)
        
        OdooController.cuentas(monto.to_i,"Cuentas por pagar","Banco")
        return [response.code, body]
        rescue
            return [500,{}]
        end
        
    end


    def self.transferirnos(monto, cuenta_origen) 
    
        url = 'http://moyas.ing.puc.cl:8080/Grupo4/integra4/banco/trx'
        
        #Se realiza la consulta.
        response = HTTParty.put(url,
        :body => { :monto => monto, :origen => cuenta_origen, :destino => "55648ad3f89fed0300525004" }.to_json,
        :headers => { "Content-Type" => "application/json"})
        begin
            body = JSON.parse(response.body)
            OdooController.cuentas(monto.to_i,"Banco","Cuentas por cobrar")
            return [response.code, body]
        rescue
            return [500,{}]
        end
        
    end    
    #Funcionando.
    #id 557b343716e7650300ede492
    def self.obtenerTransaccion(id) 
        
        url = 'http://moyas.ing.puc.cl:8080/Grupo4/integra4/banco/trx/' + id
        
        #Se realiza la consulta.
        response = HTTParty.get(url,
        :body => { }.to_json,
        :headers => { "Content-Type" => "application/json"})
        
        begin
            body = JSON.parse(response.body)
        
            return [response.code, body]
        rescue
            return [500,{}]
        end
       
    end 
    
    #Funcionando.
    def self.obtenerCartola(inicio, fin, id, limit)
        
        url = 'http://moyas.ing.puc.cl:8080/Grupo4/integra4/banco/cartola'
        
        #Se realiza la consulta.
        response = HTTParty.post(url,
        :body => { :inicio => inicio, :fin => fin, :id => id, :limit => limit }.to_json,
        :headers => { "Content-Type" => "application/json"})
        
        body = JSON.parse(response.body)
        
        return [response.code, body]
        
    end
    
    #Funcionando.
    def self.obtenerCuenta()

        url = 'http://moyas.ing.puc.cl:8080/Grupo4/integra4/banco/cuenta/556489daefb3d7030091bab9'
    
        #Se realiza la consulta.
        response = HTTParty.get(url)

        body = JSON.parse(response.body) 
        
        return [response.code, body]
        
    end 
    
end
