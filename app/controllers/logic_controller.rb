class LogicController < ApplicationController
    
    
    #Método que recibe el pedido y verifica si se podrá procesar o no.
    def self.recepcionarOrden(buyOrderId)
        
        #Se busca la orden en Atenea.
        bO = AteneaOcController.obtenerOC(buyOrderId)
        puts bO.inspect
        
        #Se busca la orden interna.
        order = BuyOrder.find_by_order_id(buyOrderId)
        #verifica si se recibieron correctamente los parámetros del servidor
        if bO[0] == 202
          if bO[1]!=[]
            #Verificar si alcanzamos a producir.
            if verificarFactibilidad(bO[1][0]["fechaEntrega"],bO[1][0]["cantidad"],bO[1][0]["sku"], buyOrderId)[0] == false
                
                #Se rechaza la orden
                json = AteneaOcController.rechazarOC(buyOrderId, "Muchos productos compadre") 
                puts json.inspect
                
                #Si falla el request a Atenea.
                if json[0] != 202  
                    puts "No se rechazó la orden de compra, revisar request"
                
                else
                    #Se le cambia el estado.
          	        if order != nil
          	            order.estado = 'rechazada'
          	            order.save                    
                    end
                
                    #Se notifica al grupo.
                    if order.cliente == "3"
                        ApiG3Controller.order_rejected(buyOrderId)
                        l = Log.new()
                        l.message = "Grupo 3 notificado de rechazo por falta de capacidad."
                        l.buy_order = order
                        l.save()
                    elsif order.cliente == "5"
                        ApiG5Controller.order_rejected(buyOrderId)
                        l = Log.new()
                        l.message = "Grupo 5 notificado de rechazo por falta de capacidad."
                        l.buy_order = order
                        l.save()
                    elsif order.cliente == "8"
                        ApiG8Controller.order_rejected(buyOrderId)
                        l = Log.new()
                        l.message = "Grupo 8 notificado de rechazo por falta de capacidad."
                        l.buy_order = order
                        l.save()
                    else
                        puts "Si estamos acá hay algo mal"
                    end
    
                 end
                
                #Aquí terminará el proceso en caso de haber rechazado la orden.
                return false
            end
          end
        end
       
        #Se acepta la orden.
        json2 = AteneaOcController.recepcionarOC(buyOrderId) 
        
        if json2[0] != 202  
        
            puts "No se aceptó la orden de compra, revisar request"
            
        else
            
            #Se busca la orden interna.
            ocId = buyOrderId
            order = BuyOrder.find_by_order_id(ocId)
        
            #Se le cambia el estado.
      	    if order != nil
      	        order.estado = 'aceptada'
      	        order.save
            end
            
                if order.cliente == "3"
                    ApiG3Controller.order_accepted(ocId)
                    l = Log.new()
                    l.message = "Grupo 3 notificado de orden aceptada."
                    l.buy_order = order
                    l.save()
                elsif order.cliente == "5"
                    ApiG5Controller.order_accepted(ocId)
                    l = Log.new()
                    l.message = "Grupo 5 notificado de orden aceptada."
                    l.buy_order = order
                    l.save()
                elsif order.cliente == "8"
                    ApiG8Controller.order_accepted(ocId)
                    l = Log.new()
                    l.message = "Grupo 8 notificado de orden aceptada."
                    l.buy_order = order
                    l.save()
                end
        end     
        
        #Aquí seguirá a Preparar Pedido, en caso de aceptar la oc.
        return true 
        
    end
    
    
    #Método que prepara los productos y despacha si es que hay stock (si no hay materias primas compra a terceros )
    def self.prepararPedido(cantidad, skuId, canal, ocId, destino, precio)
        
        #Se verifica cuanto existe en bodega.
        cantidad = cantidad.to_i
        cantidadDisponible = CellarsController.getCantidadSKU(skuId,"bodega1") + CellarsController.getCantidadSKU(skuId,"bodega2") + CellarsController.getCantidadSKU(skuId,"pulmon")
        
        #Si no existe suficiente, hay que producir.
        if cantidad > cantidadDisponible 
            
            #Se determina la cantidad faltante para producir.
            cantidadAProducir = (cantidad - cantidadDisponible)
            
            #Se recupera la orden desde la base interna.
            order = BuyOrder.find_by_order_id(ocId)
            l = Log.new()
            l.message = "Producto no suficiente en bodega, se procedió a producir más."
            l.buy_order = order
            l.save()
            
            #Si es una materia prima.
            if(order.sku =="13" ||order.sku =="19" ||order.sku =="21" )
                
                #Se produce.
                CellarsController.producir(order.sku, cantidadAProducir)
                
                #Se le cambia el estado.  (Para manejo interno se agregan dos estados a la OC: pendiente y abasteciendose)
      	        if order != nil
      	            order.estado = 'pendiente'
      	            order.save 
                    l = Log.new()
                    l.message = "Cambio de estado de Orden a pendiente"
                    l.buy_order = order
                    l.save()
      	        end 
            
            #Si requiere materias primas para ser producido.
            else
                
                #Se revisa si tengo las materias primas suficientes para producir. 
                puts '--------CANTIDAAAAAAAD-------'
                puts "cantidad:"
                puts cantidad
                #Se revisa la cantidad existente de la materia prima y del producto a generar.
                cantidadQueTengoMP = (CellarsController.getCantidadSKU(getInfoSKU(order.sku)[3],"bodega1") + CellarsController.getCantidadSKU(getInfoSKU(order.sku)[3],"bodega2") + CellarsController.getCantidadSKU(getInfoSKU(order.sku)[3],"pulmon"))
                cantidadQueTengoOrden = (CellarsController.getCantidadSKU(order.sku,"bodega1") + CellarsController.getCantidadSKU(order.sku,"bodega2") + CellarsController.getCantidadSKU(order.sku,"pulmon"))
                puts "cantidadQueTengoMP:"
                puts cantidadQueTengoMP
                puts "cantidadQueTengoOrden:"
                puts cantidadQueTengoOrden
                #Así, se calcula la cantidad faltante de la orden y se ve lo falta en materia prima.
                cantidadQueMeFaltaOrden = cantidad - cantidadQueTengoOrden
                puts "cantidadQueMeFaltaOrden:"
                puts cantidadQueMeFaltaOrden
                
                #De esta forma se saben los lotes que se necesitan en la orden.
                lotesAproducirOrden = ((cantidadQueMeFaltaOrden).to_f)/((getInfoSKU(order.sku)[1]).to_f)
                puts "lotesAproducirOrden1:"
                puts lotesAproducirOrden
                
                #Lotes que necesito producir
                if lotesAproducirOrden % 1 != 0
                    lotesAproducirOrden = lotesAproducirOrden.to_i
                    lotesAproducirOrden += 1
                end
                
                puts "lotesAproducirOrden2:"
                puts lotesAproducirOrden
                
                #Cantidad que se necesita en la orden.
                cantidadAproducirOrden = lotesAproducirOrden * getInfoSKU(order.sku)[1]
                #Cantidad que se necesita de Materia Prima.
                cantidadQueMeFaltaMP = (lotesAproducirOrden*getInfoSKU(order.sku)[1]-cantidadQueTengoOrden)* getInfoSKU(order.sku)[4] - cantidadQueTengoMP
                
                puts "cantidadAproducirOrden:"
                puts cantidadAproducirOrden
                
                puts "cantidadQueMeFaltaMP:"
                puts cantidadQueMeFaltaMP
                
                #Si hay suficiente
                if(cantidadQueMeFaltaMP <= 0) 

                    #Se produce.
                    CellarsController.producir(order.sku,cantidadAproducirOrden)
                
                    #Se le cambia el estado.  (Para manejo interno se agregan dos estados a la OC: pendiente y abasteciendose)
      	            if order != nil
      	                order.estado = 'pendiente'
      	                order.save 
                        l = Log.new()
                        l.message = "Cambio de estado de Orden a pendiente."
                        l.buy_order = order
                        l.save()
      	            end 
                    
                
                #Si no, se deben comprar materias primas.
                else
                    
                    comprarAgrupos(getInfoSKU(order.sku)[3], cantidadQueMeFaltaMP)
            
                    #Se le cambia el estado. (Para manejo interno se agregan dos estados a la OC: pendiente y abasteciendose)
      	            if order != nil
      	                order.estado = 'abasteciendose'
      	                order.save    
                        l = Log.new()
                        l.message = "Cambio de estado de Orden a abasteciéndose."
                        l.buy_order = order
                        l.save()      
                    end
                   
                end
                
            end 
            
        #Si existe cantidad suficiente en la bodega, se despacha.    
        else
            
            #Si es para el canal internacional.
            if canal == "ftp"
      	        result = CellarsController.send_products_cliente(skuId,destino,precio,ocId,cantidad)
      	        
                #Aquí termina el proceso, pues no se envía factura.
      	            
      	        
      	    #Si es para B2B.
            else
                result = CellarsController.send_products_b2b(skuId,destino,cantidad) 
                
                #Si se logra despachar.
      	        if result[0] == true
      	            
      	            #Se envía la factura correspondiente (solo si es FTP o B2B, para finalizar el proceso.
      	            if canal != 'b2c'
      	                FacturaController.firmarEmitir(ocId)
      	            end
      	            
                end 
            end 
            
            #Si se envió.
            if result[0] == true
                
                #Se recupera la orden desde la base interna.
                order = BuyOrder.find_by_order_id(ocId)
                
                #Se cambia el estado a la orden.
      	        if order != nil
      	            order.estado = 'finalizada'
      	            order.save    
                    l = Log.new()
                    l.message = "Cambio de estado de Orden a finalizada."
                    l.buy_order = order
                    l.save()                
                end
            end
        end
        
    end
    
    
    #Método que revisa constantemente las ordenes con estado pendiente o abasteciendose.
    def self.revisarOcPendientes() #Usa whenever.
    
        #Se aprovechará la regularidad, para ordenar constantemente la bodega.
        ###############################################################################################################################################################
        CellarsController.reordenar() 
        ###############################################################################################################################################################
        #Para cada orden interna se revisa el estado.
        BuyOrder.all.each do |oC|
            
            #Se revisa la fecha de entrega y se compara con la actual.
            date =Time.now+1.week
            if date < Time.now && oC.estado != "rechazada" && oC.estado != "finalizada"
                
                #Se anula la orden internamente.
                oC.estado = 'rechazada'
                oC.save
                
                #Se anula a través de atenea.
                AteneaOcController.anularOC(oC.order_id, "Tiempo de entrega ya pasó")
                l = Log.new()
                l.message = "Tiempo de entrega ya pasó. Cambio de estado a rechazada a través de atenea"
                l.buy_order = oC
                l.save()
            end     
            
            #Si es una orden pendiente.
      	    if oC.estado == 'pendiente'
      	        
      	        #Se ve la cantidad disponible en bodega.
      	        cantidadDisponible = CellarsController.getCantidadSKU(oC.sku,"bodega1").to_i + CellarsController.getCantidadSKU(oC.sku,"bodega2").to_i + CellarsController.getCantidadSKU(oC.sku,"pulmon").to_i
      	       
      	        #Si la cantidad disponible es mayor a la que se necesita.
      	        if cantidadDisponible.to_i > oC.cantidad.to_i
      	            
      	            #Si es canal internacional.
      	            if(oC.canal == "ftp")
      	                puts "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
      	                puts oC.sku
      	                puts oC.destino_id
      	                puts oC.precio_unitario
      	                puts oC.order_id
      	                puts oC.cantidad
      	                result = CellarsController.send_products_cliente(oC.sku,"direccionftp",oC.precio_unitario,oC.order_id,oC.cantidad)
      	                
      	                #Aquí termina el proceso, pues no se envía factura.
                   
                    #Si es B2B.
                    else
                        
                        puts "#########################################################"
                        result = CellarsController.send_products_b2b(productId,destino,cantidad) 
                        
                        #Si se logra despachar.
      	                if result[0] == true
      	            
      	                    #Se envía la factura correspondiente, para finalizar el proceso.
      	                    FacturaController.firmarEmitir(oC.order_id)
      	     
                        end 
                        
                    end 
                    
                    #Si se envió.
                    if result[0] == true
                        
                        oC.estado = 'finalizada'
                        oC.save
                        l = Log.new()
                        l.message = "Orden finalizada con éxito. Cambio de estado a finalizada"
                        l.buy_order = oC
                        l.save()
                    end
      	        end
      	    end 
      	       
      	    #Si es una orden abasteciendose.      
      	    if oC.estado == 'abasteciendose'
      	        
      	        #Se calcula la cantidad faltante para producir la orden.
      	        cantidadFaltante = (oC.cantidad.to_i - (CellarsController.getCantidadSKU(oC.sku,"bodega1").to_i + CellarsController.getCantidadSKU(oC.sku,"bodega2").to_i + CellarsController.getCantidadSKU(oC.sku,"pulmon").to_i))
      	        
      	        #Se pasa a lotes.          
      	        cantidadFaltanteEnLote = cantidadFaltante/(getInfoSKU(oC.sku)[1])
      	        
      	        #Se redondea hacia arriba.
      	        if cantidadFaltanteEnLote % 1 != 0
                    cantidadFaltanteEnLote.to_i += 1
                end
                
                #Se calcula la cantidad existente de materia prima.
      	        cantidadExistenteMP = CellarsController.getCantidadSKU(getInfoSKU(oC.sku)[3],"bodega1").to_i + CellarsController.getCantidadSKU(getInfoSKU(oC.sku)[3],"bodega2").to_i + CellarsController.getCantidadSKU(getInfoSKU(oC.sku)[3],"pulmon").to_i
      	        
      	        #El ratioMP * la cantidad faltante determina la cantidad necesaria de mateerias primas.
      	        #Si la materia prima necesaria es menor a la cantidad existente.
      	        if cantidadFaltante * getInfoSKU(oC.sku)[4] < cantidadExistenteMP 
      	                    
      	            #Se manda a producir. (producto, cantidad faltante en lote * cantidad que hay en un lote)
      	            puts "#####################" + oC.sku + "cantidadFaltante " +cantidadFaltanteEnLote * getInfoSKU(oC.sku)[1]+"#########################"
      	            puts "#######################################################"
      	            puts "#######################################################"
      	            puts "#######################################################"
      	            puts "#######################################################"
      	            puts "#######################################################"
      	            puts "#######################################################"
      	            puts "#######################################################"
      	            puts "#######################################################"
      	            puts "#######################################################"
      	            CellarsController.producir(oC.sku, cantidadFaltanteEnLote * getInfoSKU(oC.sku)[1])
      	            oC.estado = 'pendiente' 
      	            oC.save
                    l = Log.new()
                    l.message = "Abastéciendose. Estado pendiente."
                    l.buy_order = oC
                    l.save()
      	        end 
            end
        end    
    end
    
    
    
    
    #Método que revisa si es posible atender una orden. Se usa en recepcionarOrden().
    def self.verificarFactibilidad(fecha, cantidad, productId, buyOrderId) #fecha tipo date retorna boolean,cantidadLotes
        puts fecha
        #date = DateTime.strptime(fecha, '%m.%d.%Y')
        #Se calculan los días hasta la fecha de entrega.
        horasRestantes = (fecha.to_i - Time.now.to_i)/(60*60) 
        
        puts horasRestantes
        order = BuyOrder.find_by_order_id(buyOrderId)
        
        #Si el tiempo de entrega es menor al que toma producirlo, se rechaza el pedido.
        if horasRestantes < getInfoSKU(productId)[0]
            l = Log.new()
            l.message = "Orden de Compra Rechazada: No se alcanza a producir antes de la fecha de entrega."
            l.buy_order = order
            l.save()
            return [false,0]
        end
        
        
        
        #Se calculan los lotes que se necesitan.
        cantidadLotes = (cantidad - (getCantidadSKU(productId,"bodega1")+getCantidadSKU(productId,"bodega2")+getCantidadSKU(productId,"pulmon")))/getInfoSKU(productId)[1]
        
        #Si la cantidad no es exacta, se redondea hacia arriba.       
        if cantidadLotes % 1 != 0
            cantidadLotes.to_i += 1
        end
        
        #Se calcula la cantidad de dinero necesaria para la producción.
        plata = cantidadLotes.to_i * getInfoSKU(productId)[2]
        
        #Si el monto es mayor al que se tiene en el banco, se rechaza el pedido.
        if plata > BankController.obtenerCuenta["monto"].to_i
            l = Log.new()
            l.message = "Orden de Compra Rechazada: No hay dinero suficiente para producir el pedido."
            l.buy_order = productId
            l.save()
            return [false,0]
        end
        
        #Si no existe incapacidad económica o de tiempo, se acepta el pedido.
        return [true,cantidadLotes]
        
    end
    
    
    #Método para obtener información de los SKU propios del grupo.
    def self.getInfoSKU(productId)
        
        #Si es id: 13
        if productId=="13"
            tiempoMedio = 1.7 #Tiempo medio de producción.
            lote = 400        #Cantidad del Lote.
            costo = 2780      #Costo de producción.
            mp = 0            #Materia prima necesaria.
            mpRatio = 0       #Cantidad de materia prima necesaria para producir un lote. 
            grupo = 4         #Grupo encargado de producir
            
        elsif productId =="19"
            tiempoMedio = 1.8
            lote = 200
            costo = 1917
            mp = 0
            mpRatio = 0
            grupo = 4
            
        elsif productId =="21"
            tiempoMedio = 4
            lote = 200
            costo = 2203
            mp = 0
            mpRatio = 0
            grupo = 4
            
        elsif productId =="22"
            tiempoMedio = 2.7
            lote = 400
            costo = 2629
            mp = 6
            mpRatio = 400/380
            grupo = 4
            
        elsif productId =="23"
            tiempoMedio = 2.6
            lote = 300
            costo = 2747
            mp = 8
            mpRatio = 300/309 
            grupo = 4
            
        elsif productId =="24"
            tiempoMedio = 4.5
            lote = 400
            costo = 1988
            mp = 33
            mpRatio = 400/444
            grupo = 4
        elsif productId =="6"
            tiempoMedio = 4.1
            lote = 30
            costo = 6453
            mp = 7
            mpRatio = 0
            grupo = 1
        elsif productId =="8"
            tiempoMedio = 1.7
            lote = 1
            costo = 3891
            mp = 0
            mpRatio = 0
            grupo = 2
        elsif productId =="33"
            tiempoMedio = 3.2
            lote = 1
            costo = 3332
            mp = 0
            mpRatio = 0
            grupo = 6
        end
        
        #Se devuelve la información.
        return [tiempoMedio, lote, costo, mp, mpRatio, grupo]
    end
    
    
    #Método para comprar a otros grupos.
    def self.comprarAgrupos(sku, cantidad) #REVISAR FECHA ENTREGA Y SI ESTAN BIEN LLAMADOS LOS METODOS PA LOS GRUPOS.
    
    #fechaEntrega = (Time.now().to_i+ ((LogicController.getInfoSKU(sku)[0].to_i+1)*60*60)).to_s #ARREGLAR LA FECHA, NO ME ESTA FUNCIONANDO
    
        #Se calcula la fecha de entrega.
        fechaEntrega = ((Time.now().to_i+ (5*60*60))*1000).to_s
        puts " checkpoint 0"
        puts sku
        puts " checkpoint 1"
        puts fechaEntrega
        #Se define el grupo proveedor.
        grupo = getInfoSKU(sku.to_s)[5].to_s
        precio = getInfoSKU(sku.to_s)[2].to_s
        puts " checkpoint 2"
        puts grupo
        #Se crea la orden de compra.
    
        puts " checkpoint 3"
        puts precio
        
        respuesta = AteneaOcController.crearOC("b2b", cantidad.to_s,sku, grupo, "4" , fechaEntrega, precio)
        puts "check"
        puts respuesta #+ " checkpoint 3"
        
        #Se obtiene la id de la orden.
        if respuesta[0] == 202
            id = respuesta[1]["_id"]
        end
        
        fechaEntrega = ((Time.now() + (5*60*60))).to_s.split(" ")[0]
        puts fechaEntrega = DateTime.strptime(fechaEntrega,'%Y-%m-%d').to_s.split("T")[0]
        
        #Se compra al grupo adecuado.
        if sku == "6"

            ###ApiG1Controller.new_order(id)
            
        elsif sku == "8"
          
            
        #    puts "parametrosAPIG2:" + " id = " + id + " sku = " + sku.to_s + " fecha = " + fechaEntrega + " cantidad = " + cantidad.to_s
        #    ApiG2Controller.new_user()
        #    ApiG2Controller.new_order(id, "4", "2", sku.to_s, fechaEntrega.to_s, cantidad.to_s, getInfoSKU(sku)[2].to_s)
            
        elsif sku == "33"
            
            begin
            
            #ApiG6Controller.new_order(id)
                
            rescue
            
            end
        end
    
    end

    def self.pruebawhenever
      a=ApiKey.new()
      a.username="pruebawhenever"
      a.password="pruebawhenever"
      a.save()
    end

    def self.pruebawhenever2
      a=ApiKey.new()
      a.username="pruebawhenever2"
      a.password="pruebawhenever2"
      a.save()
    end
end

