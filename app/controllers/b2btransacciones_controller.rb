class B2btransaccionesController < ApplicationController
 before_filter :restrict_access #Con esto todos los métodos de esta clase
								#Están restringidos en acceso solo a los que hagan la 
								#request con el token válido
     
     
      respond_to :json

      

      def new_order
      	#Escribir código acá para recuperar parámetros y todo.
      	#Según yo lo mejor es hacer el modelo en la base de datos porque nos
      	#Ahorramos pega en validación
      	
      	@order_id = params[:order_id]
        @destino_id = params[:destino_id]
      	@client = params[:client]
      	@proveedor = params[:provider]
      	@sku = params[:sku]
      	@date_delivery = params[:date_delivery]
      	@quantity = params[:quantity]
      	@price = params[:price]
      	
      	#PARAMETROS
      	#order_id: (string), client: (string) opcional, proveedor: (string) opcional, sku: (string) opcional, 
      	#date_delivery: (string) opcional, quantity: (int) opcional, price: (int) opcional


      	#Cuando verificamos que la consulta tiene buenos los parámetros:
      	
      	#SKU
      	#Raw Materials
      	#13 = arroz
      	#19 = semola
      	#21 = algodón
      	#Finished Goods
      	#22 = Mantequilla 
        #23 = Harina
        #24 = Tela de Seda 


        #STOCK??
        #PLAZO??
      	
      	respond_to do |format|
      	  
      		@bo = BuyOrder.new()
              @bo.order_id = @order_id
              @bo.canal = 'b2b'
              @bo.estado = 'creada'
              @bo.cliente = @client
              @bo.proveedor = @proveedor
              @bo.sku = @sku
              @bo.fecha_entrega = @date_delivery
              @bo.cantidad = @quantity
              @bo.precio_unitario = @price
              @bo.save()
          if @bo.save

	      	  if @proveedor == '4'
	          	if (@sku == '13' || @sku == '19' || @sku ==  '21' || 
	          	   @sku ==  '22' || @sku ==  '23' || @sku ==  '24')   
	          	#[CONDICIÓN] Condición que verifica la validez de la orden 
	            #format.html { redirect_to @a, notice: 'User was successfully created.' }
	              
	                l=Log.new()
                  l.message="Orden de Compra creada exitosamente vía B2B."
                  l.buy_order=@bo
                  l.save()
                    
                  OdooController.cuentas(@bo.precio_unitario.to_i*@bo.cantidad.to_i,"Cuentas por cobrar","Ventas")
	                format.json { 
	                       render :json =>{
	                       		:success => :true,
	                       		:message => "Orden de Compra creada exitosamente."
	                       }
	                      }
	             
	                ########################################################         
	                ##### ACA EMPIEZA EL PROCESO POR PARTE DE B2B ##########
	                ########################################################
	             
	                #Una vez avisado el grupo que se creó su orden de compra,
	                #Se verifica si se aceptará o no la orden.
	                seAceptaOC = LogicController.recepcionarOrden(@bo.order_id)
	             
	                #Si es aceptada.
	                if seAceptaOC == true
	                   l=Log.new()
                     l.message="Orden de Compra aceptada."
                     l.buy_order=@bo
                     l.save()
	                    #Se elige el lugar de destino de la orden de compra.
	                    if @bo.cliente == 3
	                        destino = @destino_id
                          l=Log.new()
                  l.message="Orden de Compra asignada a destino 3."
                  l.buy_order=@bo
                  l.save()
	                  
	                    elsif @bo.cliente == 5
	                        destino = @destino_id
                          l=Log.new()
                  l.message="Orden de Compra asignada a destino 5."
                  l.buy_order=@bo
                  l.save()
	                
	                    else
	                        destino = @destino_id
	                         l=Log.new()
                  l.message="Orden de Compra asignada a destino."
                  l.buy_order=@bo
                  l.save()
	                    end     
	                  
	                    #Se procede a preparar el pedido. (Internamente, emitirá la factura también.)
	                    LogicController.prepararPedido((@bo.cantidad).to_s, (@bo.sku).to_s, "b2b", (@bo.order_id).to_s, destino , (@bo.precio_unitario).to_s)
	                     l=Log.new()
                  l.message="Orden de Compra en preparación."
                  l.buy_order=@bo
                  l.save()
	                #Sino.
	                else
	                     l=Log.new()
                  l.message="Orden de Compra rechazada."
                  l.buy_order=@bo
                  l.save()
	                    #El pedido es rechazado y "LogicController.recepcionarOrden" se encargó de avisar al cliente.
	                    #Fin del proceso.
	              
	                end 
	                
	                ########################################################         
	                ##### ACA TERMINA EL PROCESO POR PARTE DE B2B ##########
	                ########################################################
	               
	              
	            else #Caso de falla de la condición
	              #format.html { render :new }
	              
	                format.json {
	                  render :json =>{
	                       		:success => :false,
	                       		:message => "Error en la Orden de Compra: Este SKU no corresponde a la producción del grupo 4.",
	                       		:skuURL => @sku
	                       } 
	                }
	             
	            end		#El @a.errors no va a funcionar a menos de que @a sea un elemento
	          			#de nuestro modelo, por eso digo que igual creemos el objeto orden
	          			#en nuestra base de datos, nos ahorraríamos pega.
	          else
	            format.json {
	                  render :json =>{
	                       		:success => :false,
	                       		:message => "Error en la Orden de Compra: El id del proveedor debe ser igual a 4.",
	                       		:proveedor => @proveedor
	                       } 
	                }
	          end 

          else
			format.json { render json: @bo.errors, status: :unprocessable_entity }
          end 
        end
      end

    
       
       
      def order_accepted
        
      	@order_id = params[:order_id]
      	
      	@Order = BuyOrder.find_by_order_id(@order_id)
      	if @Order != nil
      	@Order.estado = 'aceptada'
      	
      	@Order.save
      	l=Log.new()
                  l.message="Orden de Compra Aceptada por parte del proveedor."
                  l.buy_order=@Order
                  l.save()
      	respond_to do |format|
      	  
        	  if true
        	    format.json {
                    render :json =>{
                         		:success => :true,
                         		:message => "Muchas gracias por avisar."
                         } 
                  }
            	
            else
              
            end  
          end

        else
          respond_to do |format|
            
            if true
              format.json {
                    render :json =>{
                            :success => :false,
                            :message => "Orden no existe."
                         } 
                  }
              
            else
              
            end  
          end
        end
      end
      
      
        
      def order_canceled
        
      	@order_id = params[:order_id]
      	
      	@Order = BuyOrder.find_by_order_id(@order_id)
      	if @Order != nil
      	@Order.estado = 'anulada'
      	
      	@Order.save
      	l=Log.new()
                  l.message="Orden de Compra anulada por el proveedor."
                  l.buy_order=@Order
                  l.save()
      	respond_to do |format|
      	  
      	  if true
      	    format.json {
                  render :json =>{
                       		:success => :true,
                       		:message => "Muchas gracias por avisar."
                       } 
                }
                
              ########################################################         
	            ##### ACA EMPIEZA EL PROCESO POR PARTE DE B2B ##########
	            ######################################################## 
              
              begin
              
                #Se anula la orden para asegurarnos en caso de que el proveedor no lo haya hecho.
                AteneaOcController.anularOC(@order.order_id, "El proveedor nos canceló")  
                
              rescue
              
                #Significa que el proveedor ya la había rechazado.
              
              ensure
              
              end
                
              ########################################################         
	            ##### ACA TERMINA EL PROCESO POR PARTE DE B2B ##########
	            ########################################################   
                
          	
          else
            
          end  
        end
      
      else
        respond_to do |format|
          
          if true
            format.json {
                  render :json =>{
                          :success => :false,
                          :message => "Orden no existe."
                       } 
                }
            
          else
            
          end  
        end
      end
      
      end 
        
      def order_rejected
        
      	@order_id = params[:order_id]
      	
      	@Order = BuyOrder.find_by_order_id(@order_id)
      	if @Order != nil
      	@Order.estado = 'rechazada'
      	
      	@Order.save
      	
  l=Log.new()
                  l.message="Orden de Compra rechazada por el proveedor."
                  l.buy_order=@Order
                  l.save()
      	
      	respond_to do |format|
      	  
      	  if true
      	    format.json {
                  render :json =>{
                       		:success => :true,
                       		:message => "Muchas gracias por avisar."
                       } 
                }
                
              ########################################################         
	            ##### ACA EMPIEZA EL PROCESO POR PARTE DE B2B ##########
	            ######################################################## 
                
              begin
              
              #Se rechaza la orden para asegurarnos en caso de que el proveedor no lo haya hecho.
              AteneaOcController.rechazarOC(@order.order_id, "El proveedor nos rechazó")  
              
              rescue
              
              #Significa que el proveedor ya la había rechazado.
              
              ensure
              
              end
                
              ########################################################         
	            ##### ACA TERMINA EL PROCESO POR PARTE DE B2B ##########
	            ########################################################
          	
          else
           
            
          end  
        end
      
      else
        respond_to do |format|
          
          if true
            format.json {
                  render :json =>{
                          :success => :false,
                          :message => "Orden no existe."
                       } 
                }
            
          else
            
          end  
        end
      end
      
      end  




      def invoice_created
        #Esta consulta se llama cuando un grupo nos acepta antes de 8 dias una factura_id
        @factura_id = params[:invoice_id]
        
        @Fact = Factura.find_by_id_fact(@factura_id)
        if @Fact != nil
        @Fact.estado_pago = 'creada'
        
        @Fact.save
        
  
        
        respond_to do |format|
          
          if true
            format.json {
                  render :json =>{
                          :success => :true,
                          :message => "Muchas gracias por avisar."
                       } 
                }
                
                
            ########################################################         
	          ##### ACA EMPIEZA EL PROCESO POR PARTE DE B2B ##########
	          ########################################################  
	          
	          #Se obtiene el grupo al que se le debe pagar.
	          grupo = @Fact.proveedor
	          
	          #Se paga la factura en el sistema.
            response = FacturaController.pagarFactura(@Fact.id_fact.to_s)
            
            if response.code == 202
                
                #Se le notifica al grupo correspondiente.
                if grupo == 1
                  
                  #Se transfiere el monto.
                  BankController.transferir(@Fact.valor_bruto.to_s, "55648ad3f89fed0300524ffe")
                  ApiG1Controller.invoice_paid(@Fact.id_fact.to_S, "")
                
                elsif grupo == 2
                
                  #Se transfiere el monto.
                  BankController.transferir(@Fact.valor_bruto.to_s, "55648ad3f89fed0300525000")
                  ApiG2Controller.invoice_paid(@Fact.id_fact.to_S, "")
                
                elsif grupo == 6
                  
                  begin
                  
                    #Se transfiere el monto.
                    BankController.transferir(@Fact.valor_bruto.to_s, "55648ad3f89fed0300524fff")
                    ApiG6Controller.invoice_paid()
                
                  rescue
                  
                    #Falló la transferencia con el grupo 6.
                    
                  end
                    
                end 
                
            end   
            
            ########################################################         
	          ##### ACA TERMINA EL PROCESO POR PARTE DE B2B ##########
	          ########################################################
            
          else
            
          end  
        end
        
      else
        respond_to do |format|
          
          if true
            format.json {
                  render :json =>{
                          :success => :false,
                          :message => "No existe esta factura"
                       } 
                }
            
          else
            
          end  
        end
      end
      end
        


      def invoice_paid
        
        @factura_id = params[:invoice_id]
        
        @Fact = Factura.find_by_id_fact(@factura_id)
        if @Fact != nil
        
        @Fact.estado_pago = 'finalizada'
        
        @Fact.save
        
  
        
        respond_to do |format|
          
          if true
            format.json {
                  render :json =>{
                          :success => :true,
                          :message => "Muchas gracias por avisar."
                       } 
                }
                
              ########################################################         
	            ##### ACA EMPIEZA EL PROCESO POR PARTE DE B2B ##########
	            ######################################################## 

                
              #Se revisa que haya existido el depósito. 
              BankController.obtenerCuenta()
              

              ########################################################         
	            ##### ACA TERMINA EL PROCESO POR PARTE DE B2B ##########
	            ########################################################
            
          else
            
          end  
        end
      
      else
        respond_to do |format|
          
          if true
            format.json {
                  render :json =>{
                          :success => :false,
                          :message => "No existe esta factura"
                       } 
                }
            
          else
            
          end  
        end
      end
      end
      

      def invoice_rejected
        @factura_id = params[:invoice_id]
        
        @Fact = Factura.find_by_id_fact(@factura_id)
        if @Fact != nil
        @Fact.estado_pago = 'rechazada'
        
        @Fact.save
        
  
        
        respond_to do |format|
          
          if true
            format.json {
                  render :json =>{
                          :success => :true,
                          :message => "Muchas gracias por avisar."
                       } 
                }
            
          else
            
          end  
        end
      
      else
        respond_to do |format|
          
          if true
            format.json {
                  render :json =>{
                          :success => :false,
                          :message => "No existe esta factura"
                       } 
                }
            
          else
            
          end  
        end
      end
      end


      def check_order(order_id)
    		  uri = URI("http://chiri.ing.puc.cl/atenea"+"/obtener/"+order_id)
    		  @header = {'Content-Type' =>'application/json'}
    		  resp = HTTParty.get(uri,:headers => @header)	
    		  @response = JSON.parse(resp.body)
    		  if @response[0]['msg'] == "Id inválido"
    			  return @response[0]['msg']
    		  else
    			  return order_id
    		  end
      end


      
      

	private
      def restrict_access
        api_key = ApiKey.find_by_access_token(params[:token])
        #head :unauthorized unless api_key
        respond_to do |format|
          
          
          if params[:token] == nil
            format.json {
                  render :json =>{
                          :success => :false,
                          :detail => "Authentication credentials were not provided."

                       } 
                }
          elsif api_key == nil
            format.json {
                  render :json =>{
                          :success => :false,
                          :detail => "Invalid token."

                       } 
                }

          else
            return
          end  
        end
      end
end
