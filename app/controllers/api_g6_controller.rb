class ApiG6Controller < ApplicationController

#--------------PROVEEDOR--------------

    require 'uri'
    
    #Error en integra6

    #---Funciona---
    #Si el usuario o el password están en la base de datos tira error 422
    #Crear usuario grupo4 en la API del G6
    def self.new_user
        host = "http://integra6.ing.puc.cl"
        uri = URI(host+"/b2b/new_user/")
        #headers = {"Content-Type" => "application/json", "Accept" => "application/json"}
        query = {"username" => "grupo4" , "password" => "1234"}
        data = HTTParty.post(uri, :query => query)
        #hash = JSON.parse(data.body)
        
        #token = hash["token"]
        
        
		return data
    end
    
    #---Funciona---
    #Obetener el token para el usuario grupo6. Token = e88437058ad90a17db1e3113fa7beccf
    def self.get_token
        host = "http://integra6.ing.puc.cl"
        uri = URI(host+"/b2b/get_token/")
        #headers = {"Content-Type" => "application/json", "Accept" => "application/json"}
        query = {"username" => "grupo4" , "password" => "1234"}
        data = HTTParty.post(uri, :query => query)
        hash = JSON.parse(data.body)
        
        token = hash["token"]
        
        
		return token
    end
    
    #---Funciona---
    #La orden se ha recibido correctamente
    #Notificar que "creamos" una orden para el G6.
    def self.new_order(order_id)
        host = "http://integra6.ing.puc.cl"
        uri = URI(host+"/b2b/new_order/")
        token = get_token
        headers = {"Authorization" =>"Token token="+ token}
        body = {"order_id" => order_id}
        data = HTTParty.post(uri, :headers => headers, :body => body)
        
        return data
    end
    
    
    
    #---Funciona---
    #"message"=>"Notificación recibida con éxito."
    #Notificar que "cancelamos" una orden realizada por nosotros al G6
    def self.order_canceled(order_id)
        host = "http://integra6.ing.puc.cl"
        uri = URI(host+"/b2b/order_canceled/")
        token = get_token
        headers = {"Authorization" =>"Token token="+ token}
        body = {"order_id" => order_id}
        data = HTTParty.post(uri, :headers => headers, :body => body)
        
        return data
    end
    
    
    #Notificar que "creamos" una factura para el G1
    def self.invoice_created(invoice_id)
        host = "http://integra6.ing.puc.cl"
        uri = URI(host+"/b2b/invoice_created/")
        token = get_token
        headers = {"Authorization" =>"Token token="+ token}
        body = {"invoice_id" => invoice_id}
        data = HTTParty.post(uri, :headers => headers, :body => body)
        
        return data
    end
    
    
    #Notificar que "rechazamos" una factura para el G1
    def self.invoice_rejected(invoice_id)
        host = "http://integra6.ing.puc.cl"
        uri = URI(host+"/b2b/order_canceled/")
        token = get_token
        headers = {"Authorization" =>"Token token="+ token}
        body = {"invoice_id" => invoice_id}
        data = HTTParty.post(uri, :headers => headers, :body => body)
        
        return data
    end

end
