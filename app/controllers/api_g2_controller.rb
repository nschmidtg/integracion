class ApiG2Controller < ApplicationController

#--------------PROVEEDOR--------------

    require 'uri'

    #---Funciona---
    #Debería ser PUT, pero funciona con GET
    #Crear usuario grupo4 en la API del G2
    def self.new_user
        host = "http://integra2.ing.puc.cl"
        uri = URI(host+"/b2b/create_user")
        headers = {"Content-Type" => "application/json", "Accept" => "application/json", "username" => "grupo4", "password" => "1234"}
        data = HTTParty.get(uri, :headers => headers)
        hash = JSON.parse(data.body)
        
        token = hash["token"]
        
		return data
    end
    
    #---Funciona---
    #Debería ser POST, pero funciona con GET
    #Obetener el token para el usuario grupo4. Token = $2a$10$EZClN6wGCnFUymO0NXiMHO61AylKfa5eS.ejDEOfx2hCBUb5tzGQy
    def self.get_token
        host = "http://integra2.ing.puc.cl"
        uri = URI(host+"/b2b/get_token")
        headers = {"Content-Type" => "application/json", "Accept" => "application/json", "username" => "grupo4", "password" => "1234"}
        data = HTTParty.get(uri, :headers => headers)
        hash = JSON.parse(data.body)
        
        token = hash["token"]
        
        return token
    end
    
    #---Funciona---
    #Todos los parametros son "string"
    #order-id: (string), client: (int), provider: (int), sku: (int), delivery-date: (string, en formato YYYY-MM-DD), quantity: (int), price: (int)
    #Notificar que "creamos" una orden para el G2
    def self.new_order(order_id, client, provider, sku, delivery_date, quantity, price)
        host = "http://integra2.ing.puc.cl"
        uri = URI(host+"/b2b/new_order")
        token = get_token
        headers = {"Content-Type" => "application/json", "Accept" => "application/json", "Authorization-Token" => token , "order-id" => order_id, "client" => client , "provider" => provider, "sku" => sku, "delivery-date" => delivery_date, "quantity" => quantity, "price" => price}
        data = HTTParty.get(uri, :headers => headers)
        #hash = JSON.parse(data.body)
        
        return data
    end
    
    #---Conecta---
    #Todos los parametros son "string"
    #status: Not Accepted
    #Notificar que "aceptamos" una orden realizada por el G2
    #def self.order_accepted(order_id)
     #   host = "http://integra2.ing.puc.cl"
      #  uri = URI(host+"/b2b/order_accepted")
       # token = get_token
        #headers = {"Content-Type" => "application/json", "Accept" => "application/json", "Authorization-Token" => token , "order-id" => order_id}
        #data = HTTParty.get(uri, :headers => headers)
        #hash = JSON.parse(data.body)
        
        #return data
    #end
    
    #---Error---
    #404
    #Notificar que "cancelamos" una orden realizada por nosotros al G2
    def self.order_canceled(order_id)
        host = "http://integra2.ing.puc.cl"
        uri = URI(host+"/b2b/order_canceled")
        token = get_token
        headers = {"Content-Type" => "application/json", "Accept" => "application/json", "Authorization-Token" => token , "order-id" => order_id}
        data = HTTParty.get(uri, :headers => headers)
        #hash = JSON.parse(data.body)
        
        return data
    end
    
    #---Conecta---
    #Todos los parametros son "string"
    #status: Not Rejected
    #Notificar que "rechazamos" una orden realizada por el G2
    #def self.order_rejected(order_id)
     #   host = "http://integra2.ing.puc.cl"
      #  uri = URI(host+"/b2b/order_rejected")
       # token = get_token
        #headers = {"Content-Type" => "application/json", "Accept" => "application/json", "Authorization-Token" => token , "order-id" => order_id}
        #data = HTTParty.get(uri, :headers => headers)
        #hash = JSON.parse(data.body)
        
        #return data
    #end
    
    #---Error---
    #404
    #Notificar que "pagamos" una factura para el G2
    def self.invoice_paid(invoice_id)
        host = "http://integra2.ing.puc.cl"
        uri = URI(host+"/b2b/invoice_paid")
        token = get_token
        headers = {"Content-Type" => "application/json", "Accept" => "application/json", "Authorization-Token" => token , "invoice-id" => invoice_id}
        data = HTTParty.get(uri, :headers => headers)
        #hash = JSON.parse(data.body)
        
        return data
    end

end
