class BuyOrder < ActiveRecord::Base
	has_one :factura
	has_many :log

    validates :cliente, :presence => true
    validates :proveedor, :presence => true
    validates :sku, :presence => true
    validates :precio_unitario, :presence => true
    validates :cantidad, :presence => true
    
    
end
