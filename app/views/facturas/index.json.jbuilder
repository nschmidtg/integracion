json.array!(@facturas) do |factura|
  json.extract! factura, :id, :id_fact, :fecha_creacion, :proveedor, :cliente, :valor_bruto, :iva, :valor_total, :estado_pago, :fecha_pago, :motivo_rechazo, :motivo_anulacion
  json.url factura_url(factura, format: :json)
end
