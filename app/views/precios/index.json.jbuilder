json.array!(@precios) do |precio|
  json.extract! precio, :id, :sku, :from, :to, :channel, :price, :code
  json.url precio_url(precio, format: :json)
end
