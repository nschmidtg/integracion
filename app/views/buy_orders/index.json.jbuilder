json.array!(@buy_orders) do |buy_order|
  json.extract! buy_order, :id, :fecha_creacion, :canal, :proveedor, :cliente, :sku, :cantidad, :cantidad_despachada, :precio_unitario, :fecha_entrega, :fecha_despacho, :estado, :motivo_rechazo, :motivo_anulacion, :notas, :id_factura
  json.url buy_order_url(buy_order, format: :json)
end
