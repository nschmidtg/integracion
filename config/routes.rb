Rails.application.routes.draw do

  resources :precios

  resources :logs

  resources :facturas

  resources :sftp_orders

  resources :buy_orders

  get 'public/documentation'


  root to: "public#index"
  # This line mounts Spree's routes at the root of your application.
  # This means, any requests to URLs such as /products, will go to Spree::ProductsController.
  # If you would like to change where this engine is mounted, simply change the :at option to something different.
  #
  # We ask that you don't use the :as option here, as Spree relies on it being the default of "spree"
  mount Spree::Core::Engine, :at => '/'
          # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
  get "/bodega" => "public#bodega", as: "bodega"

  get "/b2c/comprar" => "b2c#comprar", as: "comprar"

  get "/b2c/carro" => "b2c#carro", as: "carro"

  get "/public/documentation" => "public#documentation", as: "documentation"

  get "/public/gui" => "public#gui", as: "gui"
  
  get "/public/consulta" => "public#consulta", as: "consulta"

  get "/b2b/new_user" => "b2b#new_user", as: "new_user"

  get "/b2b/get_token" => "b2b#get_token", as: "get_token"
  
  get "/buy_orders_id" => "buy_orders#id", as: "buy_orders_id"

  get "/b2b/new_order" => "b2btransacciones#new_order", as: "new_order"
  
  get "/b2b/order_rejected" => "b2btransacciones#order_rejected", as: "order_rejected"
  
  get "/b2b/order_canceled" => "b2btransacciones#order_canceled", as: "order_canceled"
  
  get "/b2b/order_accepted" => "b2btransacciones#order_accepted", as: "order_accepted"
  
  get "/b2b/invoice_created" => "b2btransacciones#invoice_created", as: "invoice_created"
  
  get "/b2b/invoice_rejected" => "b2btransacciones#invoice_rejected", as: "invoice_rejected"

  get "/b2b/invoice_paid" => "b2btransacciones#invoice_paid", as: "invoice_paid"
  
  get "/instagram" => "instagram#getauth", as: "getauth"
  
  post "/instagram" => "instagram#recieve", as: "recieve"
  
  


  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
