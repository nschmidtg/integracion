class CreateSftpOrders < ActiveRecord::Migration
  def change
    create_table :sftp_orders do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
