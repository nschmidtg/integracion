class CreatePrecios < ActiveRecord::Migration
  def change
    create_table :precios do |t|
      t.string :sku
      t.date :from
      t.date :to
      t.string :channel
      t.string :price
      t.string :code

      t.timestamps null: false
    end
  end
end
