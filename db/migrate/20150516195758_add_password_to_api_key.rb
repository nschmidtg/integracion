class AddPasswordToApiKey < ActiveRecord::Migration
  def change
  	change_table :api_keys do |t|
      t.string :password
    end
  end
end
