class AddUsernameToApiKey < ActiveRecord::Migration
  def change
  	change_table :api_keys do |t|
      t.string :username
    end
  end
end
