class ChangeIntegerToString < ActiveRecord::Migration
  def change
    reversible do |dir|
      change_table :buy_orders do |t|
        dir.up   { t.change :cantidad, :string }
        dir.down { t.change :cantidad, :integer }

        dir.up   { t.change :precio_unitario, :string }
        dir.down { t.change :precio_unitario, :integer }

        dir.up   { t.change :cantidad_despachada, :string }
        dir.down { t.change :cantidad_despachada, :integer }

        dir.up   { t.change :order_id, :string }
        dir.down { t.change :order_id, :integer }

        
      end
    end
  end
end
