class AddBuyOrderIdToFactura < ActiveRecord::Migration
  def change
  	change_table :facturas do |t|
      t.integer :buy_order_id
    end
  end
end
