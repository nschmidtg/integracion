class ChangeDaTe < ActiveRecord::Migration
  def change
    reversible do |dir|
      change_table :buy_orders do |t|
        dir.up   { t.change :fecha_despacho, :string }
        dir.down { t.change :fecha_despacho, :date }


        
      end
    end
  end
end
