class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.string :message
      t.integer :buy_order_id
      t.timestamps null: false
    end
  end
end
