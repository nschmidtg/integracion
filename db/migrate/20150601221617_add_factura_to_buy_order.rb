class AddFacturaToBuyOrder < ActiveRecord::Migration
  def change
  	change_table :buy_orders do |t|
      t.string :estado_factura
    end
  end
end
