class AddOrderidToBuyOrders < ActiveRecord::Migration
  def change
  	change_table :buy_orders do |t|
      t.integer :order_id
    end
  end
end
