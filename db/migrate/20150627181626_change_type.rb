class ChangeType < ActiveRecord::Migration
  def self.up
    rename_column :precios, :from, :desde
    rename_column :precios, :to, :hasta
  end

  def self.down
    # rename back if you need or do something else or do nothing
  end
end
