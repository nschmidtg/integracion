class ChangeBuyOrders < ActiveRecord::Migration
  def change
    reversible do |dir|
      change_table :buy_orders do |t|
        dir.up   { t.change :fecha_creacion, :string }
        dir.down { t.change :fecha_creacion, :date }

        dir.up   { t.change :fecha_entrega, :string }
        dir.down   { t.change :fecha_entrega, :date }

        
      end
    end
  end
end
