class CreateFacturas < ActiveRecord::Migration
  def change
    create_table :facturas do |t|
      t.string :id_fact
      t.date :fecha_creacion
      t.string :proveedor
      t.string :cliente
      t.integer :valor_bruto
      t.integer :iva
      t.integer :valor_total
      t.string :estado_pago
      t.date :fecha_pago
      t.string :motivo_rechazo
      t.string :motivo_anulacion

      t.timestamps null: false
    end
  end
end
