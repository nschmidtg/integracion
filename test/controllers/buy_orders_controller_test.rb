require 'test_helper'

class BuyOrdersControllerTest < ActionController::TestCase
  setup do
    @buy_order = buy_orders(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:buy_orders)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create buy_order" do
    assert_difference('BuyOrder.count') do
      post :create, buy_order: { canal: @buy_order.canal, cantidad: @buy_order.cantidad, cantidad_despachada: @buy_order.cantidad_despachada, cliente: @buy_order.cliente, estado: @buy_order.estado, fecha_creacion: @buy_order.fecha_creacion, fecha_despacho: @buy_order.fecha_despacho, fecha_entrega: @buy_order.fecha_entrega, id_factura: @buy_order.id_factura, motivo_anulacion: @buy_order.motivo_anulacion, motivo_rechazo: @buy_order.motivo_rechazo, notas: @buy_order.notas, precio_unitario: @buy_order.precio_unitario, proveedor: @buy_order.proveedor, sku: @buy_order.sku }
    end

    assert_redirected_to buy_order_path(assigns(:buy_order))
  end

  test "should show buy_order" do
    get :show, id: @buy_order
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @buy_order
    assert_response :success
  end

  test "should update buy_order" do
    patch :update, id: @buy_order, buy_order: { canal: @buy_order.canal, cantidad: @buy_order.cantidad, cantidad_despachada: @buy_order.cantidad_despachada, cliente: @buy_order.cliente, estado: @buy_order.estado, fecha_creacion: @buy_order.fecha_creacion, fecha_despacho: @buy_order.fecha_despacho, fecha_entrega: @buy_order.fecha_entrega, id_factura: @buy_order.id_factura, motivo_anulacion: @buy_order.motivo_anulacion, motivo_rechazo: @buy_order.motivo_rechazo, notas: @buy_order.notas, precio_unitario: @buy_order.precio_unitario, proveedor: @buy_order.proveedor, sku: @buy_order.sku }
    assert_redirected_to buy_order_path(assigns(:buy_order))
  end

  test "should destroy buy_order" do
    assert_difference('BuyOrder.count', -1) do
      delete :destroy, id: @buy_order
    end

    assert_redirected_to buy_orders_path
  end
end
